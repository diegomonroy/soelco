<?php

/*

Functions for my template

*/

/*
 * Function to add my styles files
 */
function my_styles_files() {
	wp_enqueue_style( 'foundation-css', get_template_directory_uri() . '/build/foundation/css/foundation.min.css', false );
	wp_enqueue_style( 'animate-css', get_template_directory_uri() . '/build/animate.css/animate.min.css', false );
	wp_enqueue_style( 'fancybox-css', get_template_directory_uri() . '/build/fancybox/jquery.fancybox.min.css', false );
	wp_enqueue_style( 'font-awesome-css', get_template_directory_uri() . '/build/font-awesome/css/font-awesome.min.css', false );
	if ( is_child_theme() ) {
		wp_enqueue_style( 'parent-css', trailingslashit( get_template_directory_uri() ) . 'style.css', false );
	}
	wp_enqueue_style( 'theme-css', get_stylesheet_uri(), false );
}
add_action( 'wp_enqueue_scripts', 'my_styles_files' );

/*
 * Function to add my scripts files
 */
function my_scripts_files() {
	//wp_deregister_script( 'jquery' );
}
add_action( 'wp_enqueue_scripts', 'my_scripts_files' );

/*
 * Function to add my scripts files in footer
 */
function my_scripts_files_footer() {
	//wp_enqueue_script( 'jquery-js', get_template_directory_uri() . '/build/foundation/js/vendor/jquery.js', false );
	wp_enqueue_script( 'what-input-js', get_template_directory_uri() . '/build/foundation/js/vendor/what-input.js', false );
	wp_enqueue_script( 'foundation-js', get_template_directory_uri() . '/build/foundation/js/vendor/foundation.min.js', false );
	wp_enqueue_script( 'fancybox-js', get_template_directory_uri() . '/build/fancybox/jquery.fancybox.min.js', false );
	wp_enqueue_script( 'wow-js', get_template_directory_uri() . '/build/wow/wow.min.js', false );
	wp_enqueue_script( 'theme-js', get_template_directory_uri() . '/build/app.js', false );
}
add_action( 'wp_footer', 'my_scripts_files_footer' );

/*
 * Function to register my menus
 */
function register_my_menus() {
	register_nav_menus(
		array(
			'cart-menu' => __( 'Cart Menu' ),
			'main-menu' => __( 'Main Menu' ),
			'product-menu' => __( 'Product Menu' )
		)
	);
}
add_action( 'init', 'register_my_menus' );

/*
 * Function to add theme support
 */
add_theme_support( 'post-thumbnails' );

/*
 * Function to register my sidebars and widgetized areas
 */
function arphabet_widgets_init() {
	register_sidebar(
		array(
			'name' => 'Top 1',
			'id' => 'top_1',
			'before_widget' => '<div class="moduletable_to11">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Logo',
			'id' => 'logo',
			'before_widget' => '<div class="moduletable_to1 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Top 2',
			'id' => 'top_2',
			'before_widget' => '<div class="moduletable_to2">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Search',
			'id' => 'search',
			'before_widget' => '<div class="moduletable_to32">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Inicio',
			'id' => 'banner_inicio',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Construcción',
			'id' => 'banner_construccion',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Eléctricos',
			'id' => 'banner_electricos',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Herramientas',
			'id' => 'banner_herramientas',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Hogar',
			'id' => 'banner_hogar',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Left',
			'id' => 'left',
			'before_widget' => '<div class="moduletable_le1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 1',
			'id' => 'block_1',
			'before_widget' => '<div class="moduletable_b11">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 2',
			'id' => 'block_2',
			'before_widget' => '<div class="moduletable_b21">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 3',
			'id' => 'block_3',
			'before_widget' => '<div class="moduletable_b31">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Bottom',
			'id' => 'bottom',
			'before_widget' => '<div class="moduletable_bo1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
}
add_action( 'widgets_init', 'arphabet_widgets_init' );

/*
 * Function to declare WooCommerce support
 */
function woocommerce_support() {
	add_theme_support( 'woocommerce' );
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
}
add_action( 'after_setup_theme', 'woocommerce_support' );

/*
 * Functions to declare WooCommerce image sizes
 */
function woocommerce_catalog_image( $size ) {
	return array(
		'width' => 220,
		'height' => 220,
		'crop' => 1,
	);
}
add_filter( 'woocommerce_get_image_size_thumbnail', 'woocommerce_catalog_image' );

function woocommerce_single_image( $size ) {
	return array(
		'width' => 600,
		'height' => 600,
		'crop' => 1,
	);
}
add_filter( 'woocommerce_get_image_size_single', 'woocommerce_single_image' );

function woocommerce_gallery_image( $size ) {
	return array(
		'width' => 200,
		'height' => 200,
		'crop' => 1,
	);
}
add_filter( 'woocommerce_get_image_size_gallery_thumbnail', 'woocommerce_gallery_image' );

/*
 * Function to declare WooCommerce products per page
 */
function new_loop_shop_per_page( $cols ) {
	$cols = 20;
	return $cols;
}
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

/*
 * Custom single product in WooCommerce
 */
function custom_single_product() {
	global $product;
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );
	echo '<div class="single_title"><h1>';
	the_title();
	echo '</h1></div>';
	echo '<div class="single_price">' . $product->get_price_html() . '</div>';
	if ( $product->get_stock_quantity() > 1 ) {
		$stock = '<div class="single_available"><i class="fa fa-check" aria-hidden="true"></i> Producto disponible</div>';
	} elseif ( $product->get_stock_quantity() == 1 ) {
		$stock = '<div class="single_available"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Últimas unidades disponibles</div>';
	}
	if ( $product->is_in_stock() ) {
		echo '<div class="single_cart">';
		echo '<div class="left"><strong>Cantidad:</strong></div>';
		echo '<div class="right">';
		do_action( 'woocommerce_simple_add_to_cart' );
		echo '</div>';
		echo '<div class="clear"></div>';
		echo '
			<div class="single_logos">
				<div class="row collapse">
					<div class="small-12 columns">
						<p><img src="' . get_site_url() . '/wp-content/uploads/Logo-ELP.png" title="En Línea Pagos" alt="En Línea Pagos"></p>
					</div>
				</div>
				<div class="row collapse">
					<div class="small-12 medium-2 columns">
						<p><img src="' . get_site_url() . '/wp-content/uploads/Logo-Visa.png" title="Visa" alt="Visa"></p>
					</div>
					<div class="small-12 medium-2 columns">
						<p><img src="' . get_site_url() . '/wp-content/uploads/Logo-Amex.png" title="Amex" alt="Amex"></p>
					</div>
					<div class="small-12 medium-2 columns">
						<p><img src="' . get_site_url() . '/wp-content/uploads/Logo-MasterCard.png" title="MasterCard" alt="MasterCard"></p>
					</div>
					<div class="small-12 medium-2 columns">
						<p><img src="' . get_site_url() . '/wp-content/uploads/Logo-Diners.png" title="Diners" alt="Diners"></p>
					</div>
					<div class="small-12 medium-2 columns">
						<p><img src="' . get_site_url() . '/wp-content/uploads/Logo-PSE.png" title="PSE" alt="PSE"></p>
					</div>
					<div class="small-12 medium-2 columns">
						<p><img src="' . get_site_url() . '/wp-content/uploads/Logo-Efecty.png" title="Efecty" alt="Efecty"></p>
					</div>
				</div>
			</div>
		';
		echo '</div>';
		echo $stock;
	} else {
		echo '<div class="single_not_available"><i class="fa fa-times" aria-hidden="true"></i> Producto no disponible</div>';
	}
}
add_action( 'woocommerce_single_product_summary', 'custom_single_product', 1 );

function custom_single_product_bottom() {
	remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
	//remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
	echo '<div class="single_share">';
	do_action( 'woocommerce_template_single_sharing' );
	echo '</div>';
	echo '
		<div class="clear"></div>
		<h3 class="title_description">DESCRIPCIÓN DEL PRODUCTO</h3>
	';
	the_content();
	echo '
		<div class="clear"></div>
		<div class="text-center">
			<button class="hollow button" onclick="window.history.go(-1);">Volver</button>
		</div>
	';
}
add_action( 'woocommerce_after_single_product_summary', 'custom_single_product_bottom', 1 );

/*
 * Custom loop product in WooCommerce
 */
function custom_loop_product() {
	global $woocommerce_loop;
	$loop = array( '', 'featured_products', 'best_selling_products', 'sale_products', 'related' );
	if ( in_array( $woocommerce_loop['name'], $loop ) ) {
		remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
		remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );
		remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );
		remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );
		remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
		remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
		remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );
		remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
		add_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 3 );
		add_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_title', 4 );
		add_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_price', 5 );
		add_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_thumbnail', 6 );
		add_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_close', 7 );
		add_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 8 );
	}
}
add_action( 'woocommerce_before_shop_loop_item', 'custom_loop_product', 1 );

function custom_loop_link() {
	global $woocommerce_loop, $product;
	$loop = array( '', 'featured_products', 'best_selling_products', 'sale_products', 'related' );
	if ( in_array( $woocommerce_loop['name'], $loop ) ) {
		$html = '<div class="loop_link"><a href="' . get_permalink( $product->id ) . '"><i class="fa fa-plus" aria-hidden="true"></i></a></div>';
		echo $html;
	}
}
add_action( 'woocommerce_before_shop_loop_item', 'custom_loop_link', 2 );

function custom_loop_rating() {
	global $woocommerce_loop, $product;
	$loop = array( '', 'featured_products', 'best_selling_products', 'sale_products', 'related' );
	if ( in_array( $woocommerce_loop['name'], $loop ) ) {
		$average = $product->get_average_rating();
		$html = '<div class="star-rating"><span style="width: ' . ( ( $average / 5 ) * 100 ) . '%;"><strong itemprop="ratingValue" class="rating">' . $average . '</strong> ' . __( 'out of 5', 'woocommerce' ) . '</span></div>';
		echo $html;
	}
}
add_action( 'woocommerce_before_shop_loop_item', 'custom_loop_rating', 3 );

function custom_loop_available() {
	global $woocommerce_loop, $product;
	$loop = array( '', 'featured_products', 'best_selling_products', 'sale_products', 'related' );
	if ( in_array( $woocommerce_loop['name'], $loop ) ) {
		if ( $product->is_in_stock() ) {
			if ( $product->get_stock_quantity() > 1 ) {
				echo '<div class="loop_available"><i class="fa fa-check" aria-hidden="true"></i> Producto disponible</div>';
			} elseif ( $product->get_stock_quantity() == 1 ) {
				echo '<div class="loop_available"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Últimas unidades disponibles</div>';
			}
		} else {
			echo '<div class="loop_not_available"><i class="fa fa-times" aria-hidden="true"></i> Producto no disponible</div>';
		}
	}
}
add_action( 'woocommerce_before_shop_loop_item', 'custom_loop_available', 9 );

/*
 * Custom shortcode to Log In
 */
function shortcode_log_in() {
	$web = get_site_url() . '/';
	if ( is_user_logged_in() ) {
		global $current_user;
		get_currentuserinfo();
		$username = $current_user->user_login;
		$my_account = $web . 'mi-cuenta/';
		$my_orders = $web . 'productos/mi-cuenta/';
		$logout = $web . 'cerrar-sesion/';
		$html = 'Bienvenido/a! ' . $username . ' | <a href="' . $my_account . '">Mi Cuenta</a> | <a href="' . $my_orders . '">Mis Pedidos</a> | <a href="' . $logout . '">Cerrar Sesión</a>';
	} else {
		$login = $web . 'inicia-sesion/';
		$register = $web . 'crear-una-cuenta/';
		$html = 'Bienvenido/a! <a href="' . $login . '"><strong>Inicia sesión</strong></a> o <a href="' . $register . '"><strong>Crear una cuenta</strong></a>';
	}
	return $html;
}
add_shortcode( 'log_in', 'shortcode_log_in' );