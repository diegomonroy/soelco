<!-- Begin Top -->
	<section class="top_1" data-wow-delay="0.5s">
		<div class="row collapse align-center align-middle">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'top_1' ); ?>
			</div>
		</div>
	</section>
	<section class="top_2" data-wow-delay="0.5s">
		<div class="row collapse align-center align-middle">
			<div class="small-12 medium-3 columns text-center">
				<?php dynamic_sidebar( 'logo' ); ?>
			</div>
			<div class="small-12 medium-9 columns">
				<?php dynamic_sidebar( 'top_2' ); ?>
			</div>
		</div>
	</section>
	<section class="top_3" data-wow-delay="0.5s">
		<div class="row collapse align-center align-middle">
			<div class="small-12 medium-9 columns">
				<?php get_template_part( 'part', 'menu' ); ?>
			</div>
			<div class="small-12 medium-3 columns text-center">
				<?php dynamic_sidebar( 'search' ); ?>
			</div>
		</div>
	</section>
<!-- End Top -->