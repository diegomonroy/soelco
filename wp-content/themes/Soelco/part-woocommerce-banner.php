<!-- Begin Banner -->
	<section class="banner" data-wow-delay="0.5s">
		<div class="row collapse expanded">
			<div class="small-12 columns">
				<?php
				$category_1_id = 17;
				$category_1 = 'construccion';
				if ( is_product_category( $category_1 ) || has_term( $category_1, 'product_cat' ) || term_is_ancestor_of( $category_1_id, get_queried_object()->term_id, 'product_cat' ) ) {
					dynamic_sidebar( 'banner_construccion' );
				}
				$category_2_id = 53;
				$category_2 = 'electricos';
				if ( is_product_category( $category_2 ) || has_term( $category_2, 'product_cat' ) || term_is_ancestor_of( $category_2_id, get_queried_object()->term_id, 'product_cat' ) ) {
					dynamic_sidebar( 'banner_electricos' );
				}
				$category_3_id = 63;
				$category_3 = 'herramientas';
				if ( is_product_category( $category_3 ) || has_term( $category_3, 'product_cat' ) || term_is_ancestor_of( $category_3_id, get_queried_object()->term_id, 'product_cat' ) ) {
					dynamic_sidebar( 'banner_herramientas' );
				}
				$category_4_id = 121;
				$category_4 = 'hogar';
				if ( is_product_category( $category_4 ) || has_term( $category_4, 'product_cat' ) || term_is_ancestor_of( $category_4_id, get_queried_object()->term_id, 'product_cat' ) ) {
					dynamic_sidebar( 'banner_hogar' );
				}
				?>
			</div>
		</div>
	</section>
<!-- End Banner -->